SFT Spack repo:
===
Welcome to the SFT Spack repo. The purpose of this repo is to replicate the LCG software stack, using spack instead of lcgcmake (https://gitlab.cern.ch/sft/lcgcmake). 

# How to install
0. Make sure you have access to `/cvmfs/sw.hsf.org`: [How to mount cvmfs repositories](https://cvmfs.readthedocs.io/en/stable/cpt-quickstart.html)
1. Clone the spack fork of sft:
```
git clone https://gitlab.cern.ch/sft/spack.git
```
2. Source the setup script of spack:
```
source /path/to/spack/share/spack/setup-env.sh
```
3. Clone the sft-spack-repo:
```
git clone https://gitlab.cern.ch/sft/sft-spack-repo.git
```
If you are using a Centos7 machine (with /cvmfs/sw.hsf.org access), you should also source this script for setting paths in order to use the clingo concretizer:
```
source /path/to/sft-spack-repo/environments/lcg-common/setup_clingo_centos7.sh
```
4. Activate the environment lcg-release:
```
spack env activate -d /path/to/sft-spack-repo/environments/lcg-release/
```
5. Set the install tree root from `/cvmfs/sw.hsf.org/spackages2` to your preferred location locally by changing `config: install_tree: root` in `/path/to/sft-spack-repo/environments/lcg-common/config.yaml`.
6. Add the lcg-release repo to the environment:
```
spack repo add /path/to/sft-spack-repo/
```
7. You should now have the package `lcg-release` in your root specs, and no packages installed. This can be validated running `spack find`. If you don't have lcg-release in your root specs, you can add it:
```
spack add lcg-release
```
And if there are other packages in your root specs, you can run `spack remove <package>`.
8. Install lcg-release:
```
spack install
```

# Build caches
Build caches of the lcg-release are meant as a spack alternative to the RPMs provided in the LCG stack.

By now, there is a build cache of the lcg-release available for gcc 11.2.0. This is located in the lcg-spack project in the CERN Openstack, in an S3 bucket containter named gcc11.2.0.

You would need to export one environment variable to get reading access to the containers where the build caches are:
```
export S3_ENDPOINT_URL=https://s3.cern.ch
```
Then, setup spack and activate the environment lcg-release (see [How to install](#How-to-install) bullet points 0-7). You would need to have set up the compiler corresponding to the buildcache you are trying to install from in spack. This can be done using spack:
```
spack install <compiler-name>@<compiler-version>
spack compiler find </path/to/compiler/installation/prefix>
```
Note that gcc has the same package name in spack, while clang is called llvm. The setup of compilers in spack could cause some trouble if it is not being done the right way. A bootstrap tarball for compiler setup is being considered, but for now the compiler setup needs to be done by the user.

After the environment variables and the compiler are set, add the bucket as a mirror by the command (here with gcc 11.2.0 as an example):
```
spack mirror add <mirror-name> s3://gcc11.2.0
```
If you now run `spack buildcache list --allarch`, you should see a large bunch of packages being displayed. To be able to fetch these, you need to trust all keys in the buildcache:
```
spack buildcache keys --install --trust
```
Then, you can install all of the packages in the lcg-release from the buildcache:
```
spack install --cache-only lcg-release
```
If some packages fail, you could try to install them from source by running this after the cache-only install:
```
spack install --use-cache lcg-release
```

## Creating the build caches
Creating a build cache is pretty straightforward. Create an empty container with the naming scheme <compiler-name><compiler-version> (this scheme is just for keeping the object store tidy, and has no functional reason).

In order to get writing access, you need to set `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` in addition to `S3_ENDPOINT_URL`. Their values can be found in the CERN Openstack under `Project->API Access->View Credentials`, where `AWS_ACCESS_KEY_ID` should have the value of `EC2 Access Key`, and `AWS_SECRET_ACCESS_KEY` should have the value of `EC2 Secret Key`. Please make sure you view the credentials from the `lcg-spack` project so you get the right credentials.

Before writing to an S3 bucket, you would need boto3. The easiest way of adding it is probably through spack:
```
spack install py-boto3
spack load py-boto3
```
Then, run this inside the lcg-release environment:
```
spack install lcg-release%<compiler-name>@<compiler-version>
spack gpg init
spack gpg create "<name>" "<email>"
spack buildcache create -r -a -f --mirror-url s3://<compiler-name><compiler-version> lcg-release
spack buildcache update-index --mirror-url s3://<compiler-name><compiler-version>
```
If you have already initialized and created gpg keys, you can probably skip those steps.
One known catch: For installing packages from buildcache, the package hashes need to be identical. This implies that the spack fork and sft-spack-repo you use to install lcg-release for creating the build cache need to be from the same, unchanged branch when you install packages from the buildcache.  A small change in one package would cause a change in the package hash and for all the package hashes depending on it.
We choose to create empty containers before creating the build caches because installing caches to non-empty directories has triggered index errors in the whole buildcache.


# Create a new spack build machine

### Create Openstack instance 

First, login to https://openstack.cern.ch, and make sure you are in the lcg-spack project.

Go to Project->Volumes->Volumes and Create Volume.

If the quota is exceeded, you would either need to delete volumes (after checking that they are not attached to an instance in use) or request a quota change on the main page in openstack (https://openstack.cern.ch/hzrequests/).

Then, create a volume with a suitable name. We have chosen to create volumes of 200 GB so far, as the lcg-release bundle package is quite large.

Launch a new instance:
* Details: Fill in a name, a description, availability zone and count (number of instances). 
* Source: Set Image as your boot source. IT has prepared a selection of images. Since we are building with centos 7 machines, we choose `CC7 - x86_64`, which is pre-configured with cern login and some basic software, but not with cvmfs mounted to it.
* Flavor: Choose between m2.small, m2.medium or m2.large. We have chosen large because it has 4x the CPU cores of small, so the parallizable part of the build will be roughly 4x faster. We'd have chosen something larger if it was available (see https://clouddocs.web.cern.ch/using_openstack/vm_flavors.html).
* Key pair: Create or import a key pair for ssh connection. If you create a new one: Choose ssh key as your key type and name it. Remember to save the private key to clipboard!
* Metadata and configure don't need any extra specification.

Now you can launch the instance.

### ssh access

When the instance is launched, you need to get permission to access it from your computer.
First, cd to $HOME/.ssh make a new empty file with the same name as the key pair name. And make sure that only the owner of the file has read and write access:
```
cd $HOME/.ssh
touch <key-pair-name>
chmod 600 <key-pair-name>
```
Then, open the file and paste the private key in it. Once you have done it, the owner of the file would only need read access:
```
chmod 400 <key-pair-name>
```
Open $HOME/.ssh/config. If you don't have such a file from before, create an empty config file. Then, this should be filled in:
```
host <instance-name>
     Hostname       <isntance-name>.cern.ch
     IdentityFile   ~/.ssh/<key-pair-name>
     IdentitiesOnly yes
Host *
StrictHostKeyChecking no
```
Now you should be able to ssh to the new instance as root:
```
ssh root@<instance-name>
```
Add a user for your profile, and give it sudo access:
```
useradd -m -d /home/<username> <username>
passwd <username>
usermod -aG wheel <username>
```
Log out from root, and try to log in again as your username and see if you have sudo access.

### Attach and mount volume

You would also need to attach and mount the volume you created for the instance. Detailed instructions are located here: https://clouddocs.web.cern.ch/details/block_volumes.html. Attach the volume you made to the new instance in Project->Compute->Instances->Actions->Attach volume. Then, ssh to the instance and mount the volume:
```
mkfs -t ext4 -L <volume-name> /dev/vdb
mount -L <LABEL> /mnt
chmod +t /mnt
```

The `<LABEL>` of the volume is printed to user when you run the first of the three commands above (look for `Filesystem label=`). It should have the same name as the volume name, but e.g. `lcg-spack-centos7` got the label `lcg-spack-centos`, so be aware of this in case the label name is not found. To cross-check that the volume was successfully mounted, run `df -h` and see if the volume is in the list.

### cvmfs setup

For setting up cvmfs on the computer, look at the documentation: https://cvmfs.readthedocs.io/en/stable/cpt-quickstart.html. Since we are using a centos7, this is what you need to do (by June 2022):

```
sudo yum install https://ecsft.cern.ch/dist/cvmfs/cvmfs-release/cvmfs-release-latest.noarch.rpm
sudo yum install -y cvmfs
### Add this to the default.local (and other cvmfs repos if needed)
cat << EOF > default.local
CVMFS_REPOSITORIES=sft.cern.ch,sft-nightlies.cern.ch,sw.hsf.org,sw-nightlies.hsf.org
CVMFS_CLIENT_PROFILE=single
EOF
###
sudo mv default.local /etc/cvmfs/default.local
sudo cvmfs_config setup
systemctl restart autofs

cvmfs_config probe
```
Make sure all of the repos send OK.

### Spack prerequisites

Next step would be to install all prerequisites for spack: https://spack.readthedocs.io/en/latest/getting_started.html#system-prerequisites
* Python 2.7 is already installed to the CC7 image, and is still supported by spack
* GNU Make 3.82 is already installed to the CC7 image
* GNU patch 2.7.1 is already installed to the CC7 image
* GNU bash, version 4.2.46(2)-release (x86_64-redhat-linux-gnu) is already installed to the CC7 image
* tar (GNU tar) 1.26 is already installed to the CC7 image
* gzip 1.5 is already installed to the CC7 image
* xz (XZ Utils) 5.2.2 is already installed to the CC7 image
* file-5.11 is already installed to the CC7 image
* gnupg2-2.0.22-5.el7_5.x86_64 is already installed to the CC7 image

```
yum install gcc
yum install unzip
yum install bzip2
yum install zstd
yum install git
yum install svn
yum install hg
```
Now everything should be ready for whatever you want to use spack for!

```
git clone https://github.com/key4hep/spack.git
git clone https://gitlab.cern.ch/sft/sft-spack-repo.git
...
```
