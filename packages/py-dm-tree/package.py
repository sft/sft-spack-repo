# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *
import sys


class PyDmTree(PythonPackage):
    """tree is a library for working with nested data structures.
    In a way, tree generalizes the builtin map function which only
    supports flat sequences, and allows to apply a function to each
    "leaf" preserving the overall structure."""

    homepage = "https://github.com/deepmind/tree"

    if sys.platform == 'darwin':
        version('0.1.6', sha256='affc7a6d29442a143c60efb2f03bcb95424d4fe6168f3d31de892c1e601fa0e6',
                url='https://files.pythonhosted.org/packages/ab/58/544554374c9581eb588525d0dda5b269ecb98dd81d7d38e28fb2974d2861/dm_tree-0.1.6-cp39-cp39-macosx_10_14_x86_64.whl',
                expand=False)
    elif sys.platform.startswith('linux'):
        version('0.1.6', sha256='bd347c74254ba320d57b0e102558c1189e3b4ae1bae952f9aef156b5914567c8',
                url='https://files.pythonhosted.org/packages/50/2d/73ae7132e3e510415a95142fc758ff890fac77927044ecc26535cce50b22/dm_tree-0.1.6-cp39-cp39-manylinux2014_x86_64.whl',
                expand=False)

    depends_on('python@3.9', type=('build', 'run'))
    depends_on('py-setuptools', type='build')
    depends_on('py-six@1.12.0:', type=('build', 'run'))
